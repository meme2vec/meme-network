import scrapy

from meme_scraper import items
from meme_scraper import utils


class MemyPLSpider(scrapy.Spider):
    name = 'memy-pl'

    start_urls = [
        'https://memy.pl/tag/inne',
    ]

    def parse(self, response):
        # Get memes from current site
        entries = response.css(
            'article.meme-item.meme-preview.short-head-mobile'
        )
        for meme_entry in entries:
            yield self._parse_meme_info(meme_entry)

        # Proceed with next site
        next_site_url = utils.get_value(
            response,
            'div.content-stream-footer '
            'a.btn-next-stream:nth-child(2)::attr(href)'
        )
        if next_site_url:
            yield scrapy.Request(next_site_url, callback=self.parse)

    @staticmethod
    def _parse_meme_info(entry):
        url = utils.get_value(entry, 'h2 a::attr(href)')
        title = utils.get_value(entry, 'h2 a.post-heading::text')
        img_url = utils.get_value(entry, 'figure a img::attr(src)')
        img_alt = utils.get_value(entry, 'figure a img::attr(alt)')
        hashtags = utils.get_values(
            entry,
            'div.figure-options div.figure-tags a::text'
        )
        nb_shares = utils.get_value(
            entry,
            'div.article-intro.article-txt div.figure-options ' +
            'div.fb-share::text',
            default_value=0
        )
        return items.MemyPLMemeItem(
            url=url, title=title, img_url=img_url, img_alt=img_alt,
            hashtags=hashtags, nb_shares=nb_shares
        )
