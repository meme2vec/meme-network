# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class MemyPLMemeItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    img_url = scrapy.Field()
    img_alt = scrapy.Field()
    hashtags = scrapy.Field()
    nb_shares = scrapy.Field()


class FabrykaMemowPLMemeItem(scrapy.Item):
    url = scrapy.Field()
    template = scrapy.Field()
    img_url = scrapy.Field()
    img_alt = scrapy.Field()
    datetime = scrapy.Field()
    author = scrapy.Field()

class ImgflipENMemeItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    img_url = scrapy.Field()
    img_file = scrapy.Field()
    text = scrapy.Field()
    hashtags = scrapy.Field()
    author = scrapy.Field()
    views = scrapy.Field()
    upvotes = scrapy.Field()
    comments = scrapy.Field()

class ImgflipENCommItem(scrapy.Item):
    author = scrapy.Field()
    upvotes = scrapy.Field()
    level = scrapy.Field()